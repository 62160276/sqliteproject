
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author napas
 */
public class CreateTable {
    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;
        //Connection
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:"+dbName);
            Class.forName("org.sqlite.JDBC");
            stmt = conn.createStatement();
            String sql = "CREATE TABLE COMPANY" +
                    "(ID INT PRIMARY KEY NOT NULL,"+
                    "NAME TXET NOT NULL,"+
                    "AGE INT NOT NULL,"+
                    "ADDRESS CHAR(50),"+
                    "SALARY REAL)";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found!!");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to open database!!");
            System.exit(0);
        }

    }
}
