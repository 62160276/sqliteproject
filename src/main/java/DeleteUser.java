
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author napas
 */
public class DeleteUser {
        public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {            
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            Class.forName("org.sqlite.JDBC");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            //Delete
            stmt.executeUpdate("DELETE FROM USER where ID=2");
             conn.commit();
            //Selct
            ResultSet rs = stmt.executeQuery("SELECT * FROM USER");
            
            while(rs.next()){
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");
                System.out.println("ID = "+id);
                System.out.println("USERNAME = "+username);
                System.out.println("PASSWORD = "+password);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
    }        
}
