
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author napas
 */
public class InsertUser {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            String sql = "INSERT INTO USER(ID,USERNAME,PASSWORD)"
                    + "VALUES (1,'Napasorn','password');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO USER(ID,USERNAME,PASSWORD)"
                    + "VALUES (2,'Eye','password');";
            stmt.executeUpdate(sql);
            
            sql = "INSERT INTO USER(ID,USERNAME,PASSWORD)"
                    + "VALUES (3,'Plato Cat','password');";
            stmt.executeUpdate(sql);
            
            conn.commit();
                  
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
